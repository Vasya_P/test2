from django.contrib.auth.models import User
from django.test import TestCase


class UsersViewTests(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(
            username="user1", password="user1_password"
        )
        self.username2 = 'user2'
        self.password2 = 'user2_password'

    def test_users_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users.html')
        self.assertContains(response, self.user1.username)

    def test_add_user_success(self):
        response = self.client.post(
            '/',
            dict(
                username=self.username2,
                password1=self.password2,
                password2=self.password2
            ), follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.username2)

    def test_add_user_with_fail(self):
        error_msg = User.username.field.error_messages['unique']
        for _ in range(2):
            response = self.client.post(
                '/',
                dict(
                    username=self.username2,
                    password1=self.password2,
                    password2=self.password2
                ), follow=True
            )
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'username', error_msg)
