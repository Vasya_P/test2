from django.contrib.auth.management.commands.createsuperuser import (
    Command as CreateSuperuserCommand, PASSWORD_FIELD
)
from django.contrib.auth.password_validation import validate_password
from django.core import exceptions
from django.core.management.base import CommandError
from django.db import DEFAULT_DB_ALIAS
from django.utils.text import capfirst


class Command(CreateSuperuserCommand):
    help = 'Create user'

    def add_arguments(self, parser):
        parser.add_argument(
            '--%s' % PASSWORD_FIELD,
            help='Specifies the password for the user.',
        )
        parser.add_argument(
            '--%s' % self.UserModel.USERNAME_FIELD,
            help='Specifies the login for the superuser.',
        )

    def handle(self, *args, **options):
        verbose_field_name = self.username_field.verbose_name
        username = options[self.UserModel.USERNAME_FIELD]
        password = options[PASSWORD_FIELD]
        if username:
            error_msg = self._validate_username(username, verbose_field_name, DEFAULT_DB_ALIAS)
            if error_msg:
                self.stderr.write(error_msg)
                return
        elif username == '':
            raise CommandError('%s cannot be blank.' % capfirst(verbose_field_name))

        try:
            validate_password(password, self.UserModel(username=username))
        except exceptions.ValidationError as err:
            self.stderr.write('\n'.join(err.messages))
            return
        user_data = {
            self.UserModel.USERNAME_FIELD: username,
            PASSWORD_FIELD: password
        }
        self.UserModel.objects.create_user(**user_data)
        self.stdout.write("User created successfully.")
