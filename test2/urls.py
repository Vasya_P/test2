from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from test2.views import UsersView

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', UsersView.as_view()),
]
