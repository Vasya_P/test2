Для запуска проекта выполнить:  
`docker-compose up` 

Для создания пользователя выполнить:
 `docker-compose exec web ./manage.py createuser --username=<username> --password=<password>'`
